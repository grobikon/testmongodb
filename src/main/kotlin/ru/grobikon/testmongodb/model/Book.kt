package ru.grobikon.testmongodb.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Book")
data class Book(
    @Id
    var id: Long,
    var bookName: String,
    var authorName: String
)
