package ru.grobikon.testmongodb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestMongoDbApplication

fun main(args: Array<String>) {
    runApplication<TestMongoDbApplication>(*args)
}
