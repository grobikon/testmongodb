package ru.grobikon.testmongodb.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.grobikon.testmongodb.model.Book

interface BookRepository: MongoRepository<Book, Long> {
}