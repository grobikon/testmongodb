package ru.grobikon.testmongodb.controller

import org.springframework.web.bind.annotation.*
import ru.grobikon.testmongodb.model.Book
import ru.grobikon.testmongodb.repository.BookRepository
import java.util.*

@RestController
class BookController(
    val bookRepository: BookRepository
) {

    @PostMapping("/addBook")
    fun saveBook(@RequestBody book: Book): String {
        bookRepository.save(book)
        return "Added book with id: ${book.id}"
    }

    @GetMapping("/findAllBooks")
    fun getBooks(): List<Book> {
        return bookRepository.findAll()
    }

    @GetMapping("/getBook/{id}")
    fun getBook(@PathVariable id: Long): Optional<Book> {
        return bookRepository.findById(id)
    }

    @DeleteMapping("/delete/{id}")
    fun deleteBook(@PathVariable id: Long): String {
        bookRepository.deleteById(id)
        return "book delete with id: $id"
    }
}